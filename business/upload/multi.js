// 多文件上传
let async = require('async');
let errinfo = require('../../errinfo.js');
let responseJsonUtils = require('../../utils/responseJsonUtils.js');

let singleUploadBusiness = module.exports = {};


singleUploadBusiness.storeRecord = function (req, res, next) {
    let result = [];
    async.eachSeries(req.files, function (file, cb) {
        UploadRecord.create({
                name: file.hash,
                originalName: file.originalname,
                destination: file.destination,
                path: file.path,
                mime: file.mimetype, // mime
                ext: file.ext, //文件后缀,
                size: file.size, // 文件大小
                hash: file.hash,
            },
            function (err, uploadRecordResult) {
                if (err) {
                    cb(responseJsonUtils.json(errinfo.mongodb.internalError));
                    return;
                }
                result.push(uploadRecordResult.toJSON());
                cb();
            }
        );

    }, function (err) {
        if (err) {
            next(err);
            return;
        }
        res.status(200).json(result);
    });

};


