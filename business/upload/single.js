// 文件上传
let errinfo = require('../../errinfo.js');
let responseJsonUtils = require('../../utils/responseJsonUtils.js');

let singleUploadBusiness = module.exports = {};


singleUploadBusiness.storeRecord = function (req, res, next) {
    UploadRecord.create({
            name: req.file.hash,
            originalName: req.file.originalname,
            destination: req.file.destination,
            path: req.file.path,
            mime: req.file.mimetype, // mime
            ext: req.file.ext, //文件后缀,
            size: req.file.size, // 文件大小
            hash: req.file.hash,
        },
        function (err, uploadRecordResult) {
            if (err) {
                next(responseJsonUtils.json(errinfo.mongodb.internalError));
                return;
            }
            res.status(200).json(uploadRecordResult.toJSON());
        }
    );

};


