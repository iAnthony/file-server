let gulp = require('gulp');
let mocha = require('gulp-mocha');

require('../gulp/gulp-base');

gulp.task('check', ['set-test-node-env'], function () {

    return gulp.src([
        './check.test.js'
    ]).pipe(mocha()).on('error', function (err) {
        process.exit(1);
    }).on('end', function () {
        process.exit(0);
    });
});




