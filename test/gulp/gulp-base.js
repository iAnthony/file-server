let gulp = require('gulp');
let env = require('gulp-env');

gulp.task('set-dev-node-env', function () {
    return process.env.NODE_ENV = 'development';
});


gulp.task('set-prod-node-env', function () {
    return process.env.NODE_ENV = 'production';
});

gulp.task('set-test-node-env', function (cb) {
    process.env.NODE_ENV = 'test';
    process.env.NODE_CONFIG_DIR = '../../config';
    require('should');
    require('./../../env');
    cb();
});
