let express = require('express');
let multer = require('../../utils/multer-trs');
// let multer = require('multer');

let multiUploadBusiness = require('../../business/upload/multi');
let uploadUtils = require('../../utils/uploadUtils');

let router = module.exports = express.Router();

let upload = multer({storage: uploadUtils.storage});

/**
 * single upload file
 */
// store upload record in the db.
router.post('/', upload.array(uploadUtils.arrayField), multiUploadBusiness.storeRecord);