let express = require('express');
let multer = require('../../utils/multer-trs');
// let multer = require('multer');

let singleUploadBusiness = require('../../business/upload/single');
let uploadUtils = require('../../utils/uploadUtils');

let router = module.exports = express.Router();

let upload = multer({storage: uploadUtils.storage});

/**
 * upload file
 */
// store upload record in the db.
router.post('/', upload.single(uploadUtils.singleField), singleUploadBusiness.storeRecord);