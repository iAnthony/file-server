let express = require('express');


let picBusiness = require('../business/pic');


let router = module.exports = express.Router();


/**
 * param name
 */
router.param('name', picBusiness.paramName);

/**
 * get file
 */
// check  file exist
router.get('/:name', picBusiness.checkFileExist);
// zoom
router.get('/:name', picBusiness.zoom);