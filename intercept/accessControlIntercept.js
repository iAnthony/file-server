var errinfo = require('./../errinfo.js');
var responseJsonUtils = require('../utils/responseJsonUtils.js');
var URL = require('url');

module.exports = function (req, res, next) {
    // var url = URL.parse(req.);
    // res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
    res.header('Access-Control-Allow-Origin', '*');
    // res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers',
        'Content-Type,Content-Length, Authorization, Accept,X-Requested-With, hpc-session-id, set-cookie, requesttype');
    res.header('Access-Control-Expose-Headers',
        'Content-Type,Content-Length, Authorization, Accept,X-Requested-With, hpc-session-id, set-cookie, requesttype');
    // res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS');
    // res.header('X-Powered-By', ' 3.2.1');
    // res.header('Content-Type', 'application/json;charset=utf-8');
    // res.header('Content-Type', 'application/json');
    if (req.method == 'OPTIONS') {
        res.status(200).end(); //让options请求快速返回
    } else {
        next();
    }
    // next();
};
