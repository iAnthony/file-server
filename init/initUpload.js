let fs = require('fs');
let path = require('path');
let uploadUtils = require('../utils/uploadUtils');

let uploadDir = uploadUtils.destination;
fs.exists(uploadDir, function (exist) {
    if (exist) {
        // folder exist
        console.log('upload folder exist');
        return;
    }
    // folder not exist, create the folder public/upload
    fs.mkdir(uploadDir, function (err) {
        if (err) {
            console.log('create upload folder failure');
            return;
        }
        console.log('create upload folder success');
    });
});