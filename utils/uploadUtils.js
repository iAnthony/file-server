let multer = require('./multer-trs');
// let multer = require('multer');
let crypto = require('crypto');

let uploadUtils = module.exports = {
    singleField: 'file',
    arrayField: 'files',
    maxCount: 9,
    destination: 'upload'
};

uploadUtils.storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadUtils.destination);
    }
});
