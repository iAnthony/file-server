let stringUtils = module.exports = {};

stringUtils.startWith = function (str, prefix) {

    if (typeof prefix === 'undefined' || prefix === null) {
        return false;
    }

    str = String(str);
    prefix = String(prefix);

    let l = prefix.length;
    let i = -1;

    while (++i < l) {
        if (str.charAt(i) !== prefix.charAt(i)) {
            return false;
        }
    }

    return true;
};

stringUtils.endWith = function (str, suffix) {

    if (typeof suffix === 'undefined' || suffix === null) {
        return false;
    }

    str = String(str);
    suffix = String(suffix);

    let i = suffix.length;
    let l = str.length - i;

    while (i--) {
        if (suffix.charAt(i) !== str.charAt(l + i)) {
            return false;
        }
    }
    return true;
};

stringUtils.capitaliseFirstLetter = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

