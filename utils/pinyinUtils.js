let pinyinUtils = module.exports = {};
let pinyin = require('pinyin');
/**
 * get object format by model
 */
pinyinUtils.getModelFormat = function (chineseWords) {
    if (!chineseWords || chineseWords.length === 0) {
        return null;
    }
    let result = {};
    result.fullName = pinyin(chineseWords, {
        style: pinyin.STYLE_NORMAL,
        segment: false
    }).map(function (word) {
        return word[0];
    });
    result.fullNameWithSymbol = pinyin(chineseWords, {
        style: pinyin.STYLE_TONE2,
        segment: false
    }).map(function (word) {
        return word[0];
    });
    result.namePrefixes = pinyin(chineseWords, {
        style: pinyin.STYLE_INITIALS,
        segment: false
    }).map(function (word) {
        return word[0];
    });

    return result;
};