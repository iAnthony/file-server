let responseJsonUtils = {};
responseJsonUtils.json = function () {
    if (arguments.length === 1) {
        return {
            errinfo: arguments[0],
            status: arguments[0].httpCode || 500
        };
    } else if (arguments.length === 2) {
        if (typeof arguments[1] === 'number') {
            return {
                errinfo: arguments[0],
                status: arguments[0].httpCode || arguments[1]
            };
        } else if (typeof arguments[1] === 'object') {
            // Error
            return {
                errinfo: arguments[0],
                status: arguments[0].httpCode || 500,
                err: arguments[1]
            };
        } else {
            return {
                errinfo: arguments[0],
                status: arguments[0].httpCode || 500
            };
        }
    } else if (arguments.length === 3) {
        return {
            errinfo: arguments[0],
            status: arguments[0].httpCode || arguments[1],
            err: arguments[2]
        };
    } else if (arguments.length === 4) {
        arguments[2] = arguments[2] || new Error();
        return {
            errinfo: arguments[0],
            status: arguments[0].httpCode || arguments[1],
            err: arguments[2],
            data: arguments[3]
        };
    }

};

module.exports = responseJsonUtils;
