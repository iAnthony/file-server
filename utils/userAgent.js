let userAgent = module.exports = {};

userAgent.isWechat = function (userAgentString) {
    let ua = userAgentString.toLowerCase();
    return ua.match(/MicroMessenger/i) == 'micromessenger';
};