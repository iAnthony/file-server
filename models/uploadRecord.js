// upload file record
let mongoose = require('mongoose');

let schema = module.exports = new mongoose.Schema({
    name: String, // 文件的名字
    originalName: String, // 用户端的文件名称
    mime: String, // mime
    ext: String, //文件后缀,
    hash: String, // 文件 hash
    size: Number, // 文件大小
    url: String, // 存储文件的 URl
    encoding: String, // 编码格式
    destination: String, // 文件存储目录
    path: String, // 文件存储全名,包括路径
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    }, // 上传文件的用户
    createTime: { // 创建时间
        type: Date,
        default: Date.now,
    }
});
