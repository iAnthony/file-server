let mongoose = require('mongoose');


let tokenSchema = module.exports = new mongoose.Schema({
    access_token: String,
    expires_in: Number,
    refresh_token: String,
    openid: String,
    scope: String,
    create_at: String
});


tokenSchema.statics.getToken = function (openid, cb) {
    this.findOne({openid: openid}, function (err, result) {
        if (err) throw err;
        return cb(null, result);
    });
};

tokenSchema.statics.setToken = function (openid, token, cb) {
    // 有则更新，无则添加
    let query = {openid: openid};
    let options = {upsert: true};
    this.update(query, token, options, function (err, result) {
        if (err) throw err;
        return cb(result);
    });
};