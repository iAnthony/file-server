let env = global.env = {};

let ENV_PREFIX = 'MC_';
let config = require('config');

let lingo = require('lingo');
Object.keys(process.env).forEach(function (key) {
    if (key.indexOf(ENV_PREFIX) !== 0) {
        return;
    }
    env[lingo.camelcase(key.replace(ENV_PREFIX, '').toLowerCase().replace(/_/g, ' '))] = process.env[key];
});

let mergeObject = function (obj1, obj2) {
    Object.keys(obj2).forEach(function (key) {
        obj1[key] = obj2[key];
    });
};

mergeObject(global, require('./models'));
// console.log('NODE_ENV', process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    // global.http = require('supertest')(require('./app'));
    global.http = require('supertest')('127.0.0.1:' + config.port);
    // console.log(http.attach);
}