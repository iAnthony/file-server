文件存储服务器
=====================

用户文件文件存储

## 单个文件上传
> GET /upload/single

```
request:
文件数据(multipart/form-data)


respose:
文件存储记录(参见数据库设计 uploadRecord)
```

## 多个文件上传
> GET /upload/multi

```
request:
多个文件数据(multipart/form-data)


respose:
[文件存储记录(参见数据库设计 uploadRecord)]
```

## 获取图片
> GET /pic/:name

```
request:
name: 文件名(md5)
query: {
    slim: 有该参数说明对文件进行缩放
}

respose:
图片文件
```
