// err define
let err = {};

// redis
err.redis = {
    internalError: {msg: 'Redis 内部错误', code: '000001'}
};

// mongodb
err.mongodb = {
    internalError: {msg: 'Mongodb 内部错误', code: '000002', httpCode: 500}
};

// mongodb
err.pic = {
    notFount: {msg: '未找到文件', code: '000002', httpCode: 404}
};

module.exports = err;